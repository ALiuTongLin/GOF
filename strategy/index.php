<?php
/**
 * 策略模式
 * 定义独立的类来封装不同的算法 , 每一个算法都叫做一个策略
 * 目的:将算法的定义和使用分离开 , 也就是说将算法的行为和环境分开
 *
 * 我的理解: 当一个操作有多种处理方案的时候, 我们可以把每种方案封装起来 ,
 *          由具体的环境去决定使用何种方案
 *          省去了在代码中 一次次判断 然后做操作
 * */

class Context{
    /**
     * @var Strategy $concreteStrategy
     * */
    private $concreteStrategy;
    private $price;
    public function __construct($price)
    {
        $this->price = $price;
    }

    public function setStrategy($concreteStrategy){
        $this->concreteStrategy = $concreteStrategy;
    }

    public function algorithm(){
        return $this->concreteStrategy->compute($this->price);
    }
}
abstract class Strategy{
    abstract public function compute($price);
}

class Strategy7{
    public function compute($money){
        return $money * 0.7;
    }
}

class Strategy6{
    public function compute($money){
        return $money * 0.6;
    }
}

class Strategy5{
    public function compute($money){
        return $money * 0.5;
    }
}

class Strategy4{
    public function compute($money){
        return $money * 0.4;
    }
}

class Strategy3{
    public function compute($money){
        return $money * 0.3;
    }
}

$context = new Context(10);
# 注入具体的策略,这个策略在实际环境中是通过配置得来的
# 比如618 活动的时候 , 所有商品打六折 那么配置就会配置成 Strategy6
$context->setStrategy(new Strategy6());
echo '打折后的价格为:'.$context->algorithm();