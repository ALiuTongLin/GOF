<?php
namespace facade;
/**
 * 外观模式
 *
 * 为一组子系统提供一个统一的接口
 * */

class Encryption{
    public function encryption($str){
        return md5($str);
    }
}

class ArrayKeySort{
    public function sort($array){
        ksort($array);
        return $array;
    }
}

class ArrayToQueryStr{
    public function toStr($arr){
        return http_build_query($arr);
    }
}

class Facade{
    public function getSign($data){
        $key_sort  = new ArrayKeySort();
        $query_str =  new ArrayToQueryStr();
        $enctype = new Encryption();

        $res = $key_sort->sort($data);
        $res = $query_str->toStr($res);
        $res = $enctype->encryption($res);

        print_r($res);
    }
}

(new Facade())->getSign([
    'key'=>'1111111',
    'name'=>'2222222',
    'sex'=>'4444444444'
]);