<?php
/**
 * 单例模式
 * 太简单了 就不说了
 * 禁止实例化或者复制 , 获取实例 通过方法来限制
 * */

/**
 * 将一个对象保存一份实例 , 节省资源 , 数据共享
 *
 * 比如: 配置只有一份 , 系统信息只有一份
 * */
class Single{
    protected static $instance = null;
    private function __construct(){}
    private function __clone(){}

    /**
     * 获取实例
     * */
    public static function getInstance(){
        self::$instance === null && self::$instance = new static();
        return self::$instance;
    }

}

echo Single::getInstance() === Single::getInstance();