<?php
namespace composite;
/**
 * 组合模式,结构型模式
 *
 * 一对多 , 树形关系 , 牵一发而动全身,
 * */
class Component{
    protected $name;

    protected $childList = [];
    public function __construct($name)
    {
        $this->name = $name;
    }
    public function addChild($child){
        $this->childList[] = $child;
    }
    public function operation(){
        echo $this->name.PHP_EOL;
        foreach ($this->childList as $item){
            $item->operation();
        }
    }
}
class Leaf extends Component{
    public function operation()
    {
        echo PHP_EOL.$this->name.'底层员工不敢说话'.PHP_EOL;
    }
}
$c1 = new Component('老板');
$c2 = new Component('经理');
$c3 = new Component('员工1');
$c4 = new Component('员工2');
$c5 = new Leaf('底层员工1');
$c6 = new Leaf('底层员工2');

$c1->addChild($c2);
$c2->addChild($c3);
$c2->addChild($c4);
$c3->addChild($c5);
$c4->addChild($c6);

$c1->operation();


