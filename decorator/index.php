<?php
namespace decorator;
/**
 * 装饰器模式
 *
 * 动态的给对象添加功能
 *
 * 原始数据  加密  压缩   逆序   , 加密 逆序 , 压缩 逆序
 * 可以根据自己的业务情况 , 对数据进行装饰 ,
 *
 * 解决了 通过继承添加功能而造成子类急剧膨胀的问题 , 如果每种功能都继承一个子类 那么些类是非常庞大的
 * */
interface GetData{
    public function getData();
}

class Data implements GetData{
    protected $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData(){
        return $this->data;
    }
}

abstract class DataDecorator implements GetData{
    /**
     * @var DataDecorator $origin
     * */
    protected $origin;
    public function __construct($origin)
    {
        $this->origin = $origin;
    }
    public function show(){
        print_r($this->origin);
    }
    abstract public function getData();
}

class SortDecorator extends DataDecorator {
    public function getData()
    {
       $data = $this->origin->getData();
       ksort($data);
       return $data;
    }
}

class QueryDecorator extends DataDecorator {
    public function getData()
    {
        $data = $this->origin->getData();
        return http_build_query($data);
    }
}

class SignDecorator extends DataDecorator{
    public function getData()
    {
        $data = $this->origin->getData();
        return md5($data);
    }
}

$data = new Data([
    'd'=>'111111',
    'g'=>'222222',
    'j'=>'kkkkkk',
    'l'=>'222222555',
    'a'=>'dddddddddddd'
]);


$sort_data = new SortDecorator($data);

$query_data = new QueryDecorator($sort_data);

$sign_data = new SignDecorator($query_data);

print_r($sort_data->getData());
echo PHP_EOL;
print_r($query_data->getData());
echo PHP_EOL;
print_r($sign_data->getData());
