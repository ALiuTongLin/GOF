<?php
namespace adapter;

/**
 * 将实际业务包装一层 , 让不能适配的方法适配
 *
 * 看上去和代理有些相似
 *
 *
 * 如果你不能说话 , 只能用手语表达自己的想法 , 那么你可以请一个会手语能说话的人来帮你给看不懂手语的人表达自己的想法
 *
 * 适配器,就是为了把不能对接的两个对象通过适配器适配后完成对接
 *
 * 是对功能的一种适配加强
 * */
class ReturnJson{
    public function json(array $data){
        return json_encode($data);
    }
}

class Adapter{
    /**
     * @var ReturnJson
     * */
    protected $return_json;
    public function __construct()
    {
        $this->return_json = new ReturnJson();
    }

    public function json($data){
        $data = array($data);
        return $this->return_json->json($data);
    }
}

class Client{

    /**
     * @param string $data
     * @return false|string
     */
    public function getData($data){
        // 错误,因为该方法必须传递一个 array
        // (new ReturnJson())->json($data);

        # 用 Adapter 做一层适配,就可以正确调用了
        return (new Adapter())->json($data);
    }

    public static function create(){
        return  new self();
    }
}

echo Client::create()->getData('我说今天是个好日子!');
