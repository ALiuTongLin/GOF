<?php
/**
 * 观察者模式
 *
 * 将观察者聚合在主题业务中,
 * */
class GameGroup implements SplSubject{
    protected $status = '';
    private $group = [];
    /**
     * @inheritDoc
     */
    public function attach(SplObserver $observer)
    {
        echo $observer->getName().'加入团队!'.PHP_EOL;
        $this->group[] = $observer;
    }

    /**
     * @inheritDoc
     */
    public function detach(SplObserver $observer)
    {

    }

    /**
     * @inheritDoc
     */
    public function notify()
    {
        $this->status = '今晚聚餐';
        foreach ($this->group as $item){
            $item->update($this);
        }
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}


class GameItem implements SplObserver{
    protected $name;
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @inheritDoc
     */
    public function update(SplSubject $subject)
    {
        echo $this->name.'收到:'.$subject->getStatus().PHP_EOL;
    }
}

$group = new GameGroup();
$group->attach(new GameItem('王大锤'));
$group->attach(new GameItem('王二锤'));
$group->attach(new GameItem('王三锤'));
$group->attach(new GameItem('王四锤'));

$group->notify();