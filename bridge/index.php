<?php
namespace bridge;
/**
 * 桥接模式
 *
 * 将多维度变化的对象转化为低维度
 *
 * 笔的种类有  : 铅笔,钢笔,毛笔,圆珠笔
 * 笔能的颜色有: 红色,白色,蓝色,紫色
 *
 * 如果每一种具体的笔都去构建一个类 , 那么类的数量会急剧增加 , 说不一定会出现 笔的材质
 *
 * 所以我们应当找到核心 , 笔应当有多个属性, 种类 , 颜色, 以后不管怎么变化 , 笔还是笔 , 只不过是他的种类或者颜色变了
 *
 * 简单来说: 将多维度变化转化成低维度
 *
 * 一个类存在两个（或多个）独立变化的维度，且这两个（或多个）维度都需要独立进行扩展。
 * */

/**
 * 再看桥接模式:
 *  将抽象部分和实现部分 分离,使他们都可以独立的变化,
 * */
abstract class Pen{
    protected $name = '我是一只抽象笔';
    /**
     * @var Color
     * */
    protected $color;

    public function __construct($color)
    {
        $this->color = $color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function show(){
        echo $this->name.'  颜色为:'.$this->color->getName().PHP_EOL;
    }
    public static function getInstance(Color $color){
        return new static($color);
    }
}

abstract class Color{
    protected $name = '我是一种抽象颜色';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}


class 铅笔 extends Pen{
    protected $name = '我是铅笔';
}
class 钢笔 extends Pen{
    protected $name = '我是钢笔';
}
class 圆珠笔 extends Pen{
    protected $name = '我是圆珠笔';
}

class 红色 extends Color{
    protected $name = '红色';
}
class 白色 extends Color{
    protected $name = '白色';
}
class 蓝色 extends Color{
    protected $name = '蓝色';
}


钢笔::getInstance(new 白色())->show();
钢笔::getInstance(new 蓝色())->show();
钢笔::getInstance(new 红色())->show();

圆珠笔::getInstance(new 白色())->show();
圆珠笔::getInstance(new 蓝色())->show();
圆珠笔::getInstance(new 红色())->show();

铅笔::getInstance(new 白色())->show();
铅笔::getInstance(new 蓝色())->show();
铅笔::getInstance(new 红色())->show();
