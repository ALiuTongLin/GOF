<?php
/**
 * 模板方法模式
 *
 * 基类规定运转流程 , 子类实现后覆盖部分流程方法
 * 基类提供流程,子类提供具体方案
 * */

abstract class Alert{
    abstract protected function getTitle();

    protected function over(){
        return '弹出完毕';
    }

    public function show(){
        $title =  $this->getTitle();
        echo $title;
        echo $this->over();
    }
}

class ConfirmAlert extends Alert{
    protected function getTitle()
    {
        return '你想要干什么?';
    }
}


(new ConfirmAlert())->show();
