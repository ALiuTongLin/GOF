<?php
/**
 * 原型模式
 *
 * 如果两个对象差距不大 , 通过 clone 原型再修改可以提高效率和简化代码
 *
 * */

class Info{
    protected $name = '';
    protected $sex = '';
    protected $age = '';
    protected $subject = '';
    protected $content = '';

    public function __construct($name,$sex,$age,$subject,$content)
    {
        $this->name = $name;
        $this->sex = $sex;
        $this->age = $age;
        $this->subject = $subject;
        $this->content = $content;
    }

    public function show()
    {
        echo $this->name.$this->sex.$this->age.$this->subject.$this->content.PHP_EOL;
    }

    public function __set($name, $value)
    {
        if(isset($this->$name)){
            $this->$name = $value;
        }
    }

    public function __get($name)
    {
        return isset($this->$name) ? $this->$name:null;
    }
}

$info = new Info('王大锤','男','18','第一次会议','这次会议的内容为:好好学习天天上当!');

// 如果后面还存在一个与之相似的对象, 他们的差距仅仅是 内容不同,那么我们可以直接克隆

$info_clone = clone $info;
$info_clone->subject = '第二次会议';

$info->show();
$info_clone->show();

/**
 * output
 * 王大锤男18第一次会议这次会议的内容为:好好学习天天上当!
 * 王大锤男18第二次会议这次会议的内容为:好好学习天天上当!
 * */