<?php
/**
 * 备忘录模式
 * 该模式可做撤销
 * 简单理解: 备忘录对象用于记录当前业务对象的状态 , 每个备忘录对应了当时的一个状态
 *          备忘录管理者用于管理备忘录 , 可以设置合适的数据结构从而实现多次撤销前进等功能
 *
 * 对对象的状态进行记录
 * */
class Woman{
    protected $status = '';
    public function go($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        echo '当前状态为:'.$this->status.PHP_EOL;
    }

    public function save()
    {
        return new Memento($this->status);
    }

    public function restore(Memento $memento)
    {
        $this->status = $memento->getState();
    }
}

class Memento{
    private $status;
    public function __construct($status)
    {
        $this->status = $status;
    }

    public function getState(){
        return $this->status;
    }
}

class MementoManager{
    private $memento;

    /**
     * @param mixed $memento
     */
    public function setMemento($memento)
    {
        $this->memento = $memento;
    }

    /**
     * @return mixed
     */
    public function getMemento()
    {
        return $this->memento;
    }
}

$woman = new Woman();
$mementoManager = new MementoManager();

$woman->go('向前走 10 步');
$woman->go('向左走 10 步');


# 保存状态
$mementoManager->setMemento($woman->save());
$woman->go('向右走 10 步');
$woman->getStatus();
# 恢复状态
$woman->restore($mementoManager->getMemento());
$woman->getStatus();
