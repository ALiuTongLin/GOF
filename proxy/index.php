<?php
namespace proxy;
/**
 * 代理模式
 *
 * 这个模式和适配器有些相似
 *
 * 但是: 代理模式中 , 代理对象 和 主题对象的接口是一致的 , 用法上没有任何区别
 *
 *      适配器 和 主题对象 的接口不一样 , 也就是说 被适配的对象没有这个功能 , 需要通过适配
 *      对象来做调整才能实现这个功能 ,
 *
 * 比如: 客户端希望服务器返回 json , 但是当前的服务器只能返回 xml, 于是为了不影响原有业务 , 对接口返回
 *       做一个适配 , 将 xml 数据转化为 json
 *
 *       某个方法不能处理 json 字符串 , 只能处理 array , 现在客户端 只能提供 json 字符串
 *       于是 , 对该方法做了一层适配 , 经过适配后 , json 将会被转化为 array , 然后 处理就没有问题了
 *
 *
 * 代理: 代理则是某个对象代替主题对象 , 那么必然这个对象拥有主题对象的一切功能 , 能调用主题对象的地方一定能调代理对象
 *      为啥不用继承呢 ?
 *      继承后改改接口一样可以实现代理呀 , 问题放在这里
 * */
interface Subject{
    public function request();
}

class Request implements Subject{
    public function request(){
        echo '发送请求到 baidu.com'.PHP_EOL;
    }
}

class RequestProxy implements Subject{
    protected $request;
    public function __construct()
    {
        $this->request = new Request();
    }
    public function request()
    {
        echo '开始代理请求'.PHP_EOL;
        $this->request->request();
        echo '代理请求结束'.PHP_EOL;
    }
}

$request = new RequestProxy();
$request->request();