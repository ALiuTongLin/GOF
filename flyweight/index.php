<?php
namespace flyweight;

/**
 * 享元工厂
 *
 * 有些类似于单例模式 , 单例模式是当前对象只有一个实例
 * 享元模式则是 , 管理的对象只有一个实例 , 将多个对象管理起来 , 保存一份实例, 当需要的时候直接获取该实例
 * 达到共享的状态 ,
 *
 * 需要区分内部状态和外部状态 , 内部状态即: 不变的部分 , 外部状态即在程序运行过程中可以根据环境变化的部分
 * 减少内存中对象的数量
 * */
class FlyweightFactory{
    protected $hasTable = [];

    private function __construct(){
        $this->hasTable['White'] = new White();
        $this->hasTable['Red'] = new Red();
    }

    public static function getInstance(){
        return new self();
    }

    public function getObj($key){

        if(!isset($this->hasTable[$key])){
            $this->hasTable[$key] = new $key();
        }

        return $this->hasTable[$key];
    }
}

class White{
    protected $name = '白色';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

class Red{
    protected $name = '红色';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

echo FlyweightFactory::getInstance()->getObj('Red')->getName();
echo FlyweightFactory::getInstance()->getObj('White')->getName();