<?php
/**
 * 责任链模式
 * 请求链式传递模式 ,
 * 一个请求 对应 多个处理方法 , 为了让请求和处理 分开
 * */
class Client{

    /**
     * 处理者
     * @var Handle $handle
     * */
    protected $handle = null;
    /**
     * 初始化的时候,构建责任链,
     * */
    public function __construct()
    {
        # 该请求从 A 开始
        $this->handle = new HandelA();
        $this->handle->setSuccessor(new HandelB())
            ->setSuccessor(new HandelC())
            ->setSuccessor(new HandelD());
    }

    public function request($status)
    {
        $this->handle->handleRequest($status);
        echo PHP_EOL;
    }
}

abstract class Handle{
    /**
     * @var Handle $successor
     * */
    protected $successor = null;

    /**
     * 设置后继处理对象
     * @param Handle $handle
     * @return Handle
     */
    public function setSuccessor(Handle $handle)
    {
        $this->successor = $handle;

        return $handle;
    }
    abstract public function handleRequest($status);
}
// 具体处理请求
class HandelA extends Handle{
    public function handleRequest($status)
    {
        if($status < 5) {
            echo '处理人 A 对请求做出了处理!';
        }else{
            echo '处理人 A 无法正确做出处理,现在呼叫处理人 B 来处理';
            $this->successor->handleRequest($status);
        }
    }
}
class HandelB extends Handle{
    public function handleRequest($status)
    {
        if($status < 10) {
            echo '处理人 B 对请求做出了处理!';
        }else{
            echo '处理人 B 无法正确做出处理,现在呼叫处理人 C 来处理';
            $this->successor->handleRequest($status);
        }
    }
}

class HandelC extends Handle{
    public function handleRequest($status)
    {
        if($status < 15) {
            echo '处理人 C 对请求做出了处理!';
        }else{
            echo '处理人 C 无法正确做出处理,现在呼叫处理人 D 来处理';
            $this->successor->handleRequest($status);
        }
    }
}

class HandelD extends Handle{
    public function handleRequest($status)
    {
        if($status < 20) {
            echo '处理人 D 对请求做出了处理!';
        }else{
            echo '处理人 D 无法正确做出处理,请求结束';
        }
    }
}

// 构建责任链 , 在这里我选择交给客户端自己构造
// 真实场景下 , 应当交给 专门构造责任链的对象来创建 ,

// 客户端只需要发送请求 , 而无需关心谁来处理请求
// 因为这个请求会顺着责任链传递下去 , 直到请求被处理
$client = new Client();

$client->request(4);
$client->request(6);
$client->request(14);
$client->request(19);